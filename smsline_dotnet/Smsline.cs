using System;
using System.IO;
using System.Net;
using System.Text;
using System.Web;
using System.Security.Cryptography;
using Newtonsoft.Json;

namespace smsline { 
    class Options {
        public int ttl {get; set;}
        public string img {get; set;}
        public string caption {get; set;}
        public string action {get; set;}
        public string ios_expirity_text {get; set;}
    }

    class Channel {
        public string target {get; set;}
        public string text {get; set;}
        public Options options {get; set;}
    }

    class Message {
        public string target {get; set;}
        public string msisdn {get; set;}
        public string text {get; set;}
        public string external_id {get; set;}
        public string callback_url {get; set;}
        public string reply_url {get; set;}
        public Options options {get; set;}
    }

    class MessageSingle {
        public string target {get; set;}
        public string msisdn {get; set;}
        public string text {get; set;}
        public string external_id {get; set;}
        public string callback_url {get; set;}
        public string reply_url {get; set;}
        public Options options {get; set;}  
    }

    class MessageMulti {
        public string msisdn {get; set;}
        public string text {get; set;}
        public Channel[] channels {get; set;}
    }

    class BulkSingle {
        public string target {get; set;}
        public string text {get; set;}
        public Message[] messages {get; set;}
        public string callback_url {get; set;}
        public string reply_url {get; set;}
        public Options options {get; set;}
    }

    class BulkMulti {
        public Message[] messages {get; set;}
        public Channel[] channels {get; set;}
    }

    class MessagesStatus {
        public string date_end {get;set;}
        public string date_start {get;set;}
        public string id_message {get;set;}
        public int message_state {get;set;}
        public string msisdn {get;set;}
        public string target {get;set;}
    }

    public class Smsline
    {
        private string login;
        private string password;
        private string url;
        public Smsline(string login, string password, string url) {
            this.login = login;
            this.password = password;
            this.url = url;
        }

        public static string GetHash(string text, string key) {
            UTF8Encoding encoding = new UTF8Encoding();
            Byte[] textBytes = encoding.GetBytes(text);
            Byte[] keyBytes = encoding.GetBytes(key);

            Byte[] hashBytes;

            using (HMACSHA256 hash = new HMACSHA256(keyBytes))
                hashBytes = hash.ComputeHash(textBytes);

            return BitConverter.ToString(hashBytes).Replace("-", "").ToLower();
        }

        private string PostRequest(string url, string data, string login, string signature) {
            var request = WebRequest.Create(url);
            request.Method = "POST";
            request.Headers.Add("Authorization-User", login);
            request.Headers.Add("Authorization", string.Format("Bearer {0}", signature));
            request.ContentType = "application/json";

            using (var streamWriter = new StreamWriter(request.GetRequestStream())) {
                streamWriter.Write(data);
                streamWriter.Flush();
                streamWriter.Close();
            }

            try {
                using (WebResponse response = request.GetResponse()) {
                    using (Stream page = response.GetResponseStream())
                    using (var reader = new StreamReader(page)) {
                        string text = reader.ReadToEnd();
                        return text;
                    }
                }
            }
            catch (WebException e) {
                using (WebResponse response = e.Response) {
                    HttpWebResponse httpResponse = (HttpWebResponse) response;
                    using (Stream page = response.GetResponseStream())
                    using (var reader = new StreamReader(page)) {
                        string text = reader.ReadToEnd();
                        return text;
                    }
                }
            }
        }

        private string GetRequest(string url, string login, string signature) {
            var request = WebRequest.Create(url);
            request.Method = "GET";
            request.Headers.Add("Authorization-User", login);
            request.Headers.Add("Authorization", string.Format("Bearer {0}", signature));

            try {
                using (WebResponse response = request.GetResponse()) {
                    using (Stream page = response.GetResponseStream())
                    using (var reader = new StreamReader(page)) {
                        string text = reader.ReadToEnd();
                        return text;
                    }
                }
            }
            catch (WebException e) {
                using (WebResponse response = e.Response) {
                    HttpWebResponse httpResponse = (HttpWebResponse) response;
                    using (Stream page = response.GetResponseStream())
                    using (var reader = new StreamReader(page)) {
                        string text = reader.ReadToEnd();
                        return text;
                    }
                }
            }
        }

        public static string JsonToString(object json) {
            return JsonConvert.SerializeObject(
                json,
                Formatting.Indented,
                new JsonSerializerSettings { 
                    DefaultValueHandling = DefaultValueHandling.Ignore 
                }
            );   
        }

        public string MessageSingleChannel(string channel, object body) {
            var requestUrl = $"{url}/v3/messages/single/{channel}";
            var requestBody = JsonToString(body);

            var signature = GetHash(
                $"messagessingle{channel}{requestBody}", 
                password
            );
            
            return PostRequest(requestUrl, requestBody, login, signature);
        }

        public string MessageMultiChannel(string channel, object body) {
            var requestUrl = $"{url}/v3/messages/multi/single/{channel}";
            var requestBody = JsonToString(body);

            var signature = GetHash(
                $"messagesmultisingle{channel}{requestBody}", 
                password
            );
            
            return PostRequest(requestUrl, requestBody, login, signature);
        }

        public string MessagesSingleChannel(string channel, object body) {
            var requestUrl = $"{url}/v3/messages/bulk/{channel}";
            var requestBody = JsonToString(body);

            var signature = GetHash(
                $"messagesbulk{channel}{requestBody}", 
                password
            );
            
            return PostRequest(requestUrl, requestBody, login, signature);
        }

        public string MessagesMultiChannel(string channel, object body) {
            var requestUrl = $"{url}/v3/messages/multi/bulk/{channel}";
            var requestBody = JsonToString(body);

            var signature = GetHash(
                $"messagesmultibulk{channel}{requestBody}", 
                password
            );

            Console.WriteLine(requestBody);
            
            return PostRequest(requestUrl, requestBody, login, signature);
        }

        public string MessageStatus(string messageId) {
            string requestUrl = $"{url}/v3/messages/{messageId}";

            var signature = GetHash(
                $"messages{messageId}", 
                password
            );

            return GetRequest(requestUrl, login, signature);
        }

        public string MessagesStatuses(object body) {
            var parametrs = JsonToString(body);
            parametrs = parametrs
                .Replace("\": \"", "=")
                .Replace("\": ", "=")
                .Replace("\",\n  \"", "&")
                .Replace(",\n  \"", "&")
                .Replace("\",\n  ", "&")
                .Replace("{\n  \"", "")
                .Replace("\"\n}", "")
                .Replace("\n}", "");

            var requestUrl = $"{this.url}/v3/messages?{parametrs}";
            var signature = GetHash(
                $"messages{parametrs.Replace("=", "").Replace("&", "")}", 
                password
            );

            return GetRequest(requestUrl, login, signature);;
        }
    }
}