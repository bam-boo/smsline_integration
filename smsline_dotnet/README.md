# SMS-line HTTP API 3.0.0

Для работы модуля необходимо добавить файл Smsline.cs в существующий проект и установить дополнительную библиотеку:
```sh
$ dotnet add package Newtonsoft.Json --version 11.0.2
```

### MessageSingleChannel
Для отправки одного сообщения через один канал нужно воспользоваться методом MessageSingleChannel (сообщение будет отправлено только через канал sms или viber). Который принимает два параметра:
  - channel, данный параметр может принимать значения "viber" или "sms"
  - body, данный параметр представляет из себя экзепляр класса в котором описаны параметры которые будут использоваться для отправки сообщения

Пример для отправки SMS:
```csharp
using System;
using smsline;

namespace smsline {
    class Program {
        static void Main(string[] args) {
            var smsline = new Smsline("username", "password", "https://api.smsline.by")

            Console.WriteLine(
                smsline.MessageSingleChannel(
                    "sms",
                    new MessageSingle {
                        target = "Target",
                        msisdn = "375XXXXXXXXX",
                        text = "some text",
                        external_id = "000000000",
                        callback_url = "https://example.com/callback"
                    }
                )
            );
        }
    }
}
```
Пример для отправки Viber сообщения:
```csharp
using System;
using smsline;

namespace smsline {
    class Program {
        static void Main(string[] args) {
            var smsline = new Smsline("username", "password", "https://api.smsline.by")

            Console.WriteLine(
                smsline.MessageSingleChannel(
                    "viber",
                    new MessageSingle {
                        target = "Target",
                        msisdn = "375XXXXXXXXX",
                        text = "viber message",
                        options = new Options {
                            ttl = 120,
                            img = "https://example.com/image.png",
                            caption = "Text on button",
                            action = "https://example.com",
                            ios_expirity_text = "Some expirity text"
                        }
                    }
                )
            );
        }
    }
}
```


### MessageMultiChannel
Для отправки одного сообщения через мультиканал нужно воспользоваться методом MessageMultiChannel (сообщение будет отправлено через первый указанный канал, в случае неудачи повторная отправка произойдет через второй указанный канал). Который принимает два параметра:
  - channel, данный параметр может принимать единственное значение "VIBER_SMS"
  - body, данный параметр представляет из себя экземпляр класса в котором описаны параметры которые будут использоваться для отправки сообщения

В таком случае вызов метода будет выглядеть следующим образом:
```csharp
using System;
using smsline;

namespace smsline {
    class Program {
        static void Main(string[] args) {
            var smsline = new Smsline("username", "password", "https://api.smsline.by")

            Console.WriteLine(
                smsline.MessageMultiChannel(
                    "VIBER_SMS",
                    new MessageMulti {
                        msisdn = "375XXXXXXXXX",
                        text = "Default text"
                        channels = new Channel[] {
                            new Channel {
                                target = "Viber target"
                                options = new Options() {
                                    ttl = 120,
                                    img = "https://example.com/image.png",
                                    caption = "Text on button",
                                    action = "https://example.com",
                                    ios_expirity_text = "Some expirity text"
                                }
                            },
                            new Channel {
                                target = "SmsTarget"
                            }
                        }
                    }
                )
            );
        }
    }
}
```
### MessagesSingleChannel
Для отправки нескольких сообщений через один канал, необходимо воспользоваться методом MessagesSingleChannel. Который принимает два параметра:
  - channel, данный параметр может принимать значения "viber" или "sms"
  - body, данный параметр представляет из себя экземпляр класса BulkSingle в котором описаны параметры которые будут использоваться для отправки сообщений

В таком случае вызов метода будет выглядеть следующим образом:
```csharp
using System;
using smsline;

namespace smsline {
    class Program {
        static void Main(string[] args) {
            var smsline = new Smsline("username", "password", "https://api.smsline.by")

            Console.WriteLine(
                smsline.MessagesSingleChannel(
                    "sms",
                    new BulkSingle {
                        target = "target",
                        text = "Default text",
                        messages = new Message[] {
                            new Message {
                                msisdn = "375XXXXXXXX1"
                            },
                            new Message {
                                msisdn = "375XXXXXXXX2",
                                text = "Custom text 1"
                            },
                            new Message {
                                msisdn = "375XXXXXXXX3",
                                text = "Custom text 2"
                            }
                        }
                    }
                )
            );
        }
    }
}
```

Отправка сообщений через "viber" канал произходит аналогичным образом.

### MessagesMultiChannel
Для отправки нескольких сообщений через мультиканал канал, необходимо воспользоваться методом MessagesMultiChannel. Который принимает два параметра:
  - channel, данный параметр может принимать значения "viber" или "sms"
  - body, данный параметр представляет из себя экземпляр класса BulkMulti в котором описаны параметры которые будут использоваться для отправки сообщений

В таком случае вызов метода будет выглядеть следующим образом:
```csharp
using System;
using smsline;

namespace smsline {
    class Program {
        static void Main(string[] args) {
            var smsline = new Smsline("username", "password", "https://api.smsline.by")

            Console.WriteLine(
                smsline.MessagesMultiChannel(
                    "VIBER_SMS",
                    new BulkMulti {
                        messages = new Message[] {
                            new Message {
                                msisdn = "375XXXXXXXX1"
                            },
                            new Message {
                                msisdn = "375XXXXXXXX2"
                            },
                            new Message {
                                msisdn = "375XXXXXXXX3"
                            }
                        },
                        channels = new Channel[] {
                            new Channel {
                                target = "Viber target",
                                text = "viber message",
                                options = new Options() {
                                    ttl = 120,
                                    img = "https://example.com/image.png",
                                    caption = "Text on button",
                                    action = "https://example.com",
                                    ios_expirity_text = "Some expirity text"
                                }
                            },
                            new Channel {
                                target = "SmsTarget",
                                text = "sms message"
                            }
                        }
                    }
                )
            );
        }
    }
}
```

### MessageStatus
Возвращает статус отправленного сообщения. Принимает значение id_message.
```csharp
Console.WriteLine(
    smsline.MessageStatus("00000000-0000-0000-0000-000000000000")
);
```

### MessagesStatuses
Возвращает статусы всех сообщений попавших под выбранный фильтр. Принимает один парамент, который является экземпляром класса MessagesStatus.

В таком случае вызов метода будет выглядеть следующим образом:
```csharp
using System;
using smsline;

namespace smsline {
    class Program {
        static void Main(string[] args) {
            var smsline = new Smsline("username", "password", "https://api.smsline.by")

            Console.WriteLine(
                smsline.MessagesStatuses(
                    new MessagesStatus {
                        date_start = "YYYY-MM-DD HH:mm:ss",
                        date_end = "YYYY-MM-DD HH:mm:ss", //разница не более одних суток
                        target = "Target",
                        msisdn = "375XXXXXXXXX",
                        message_state = 2
                    }
                )
            );
        }
    }
}
```

### GetHash
Статический метод позволяющий формировать подпись. Принимает два параметра:
- первый параметр принимает подготовленную строку
- второй параметр принимает пароль для api

Результатом выполнения будет строка, которую необходимо передать в поле Authorization, значение необходимо передавать в виде:
```csharp
var signature = GetHash("prepared string", "password")
$"Bearer {signature}"
```
