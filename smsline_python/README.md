# SMS-line HTTP API 3.0.0

Для использования с python 3 и выше.


### message_single_channel
Для отправки одного сообщения через один канал нужно воспользоваться методом message_single_channel (сообщение будет отправлено только через sms или viber). Который принимает два параметра:
  - channel, данный параметр может принимать значения "viber" или "sms"
  - body, принимает параметры, которые будут использоваться для отправки сообщения

Пример для отправки SMS:
```python
import smsline


connect = smsline.Smsline("username", "password", "https://api.smsline.by")

print(connect.message_single_channel("sms", {
    "target": "Target",
    "msisdn": "375XXXXXXXXX",
    "text": "some text",
    "external_id": "000000000",
    "callback_url": "https://example.com/callback"
}))
```

Пример для отправки Viber сообщения:
```python
import smsline


connect = smsline.Smsline("username", "password", "https://api.smsline.by")

print(connect.message_single_channel("viber", {
    "target": "Target",
    "msisdn": "375XXXXXXXXX",
    "text": "some text",
    "options": {
        "ttl": "120",
        "img": "https://example.com/image.png",
        "caption": "Text on button",
        "action": "https://example.com"
    }
}))
```

### message_multi_channel
Для отправки одного сообщения через мультиканал нужно воспользоваться методом message_multi_channel (сообщение будет отправлено через первый указанный канал, в случае неудачи повторная отправка произойдет через второй указанный канал). Который принимает два параметра:
  - channel, данный параметр может принимать единственное значение "VIBER_SMS"
  - body, принимает параметры, которые будут использоваться для отправки сообщения

Пример для отправки сообщения через multi канал:

```python
import smsline


connect = smsline.Smsline("username", "password", "https://api.smsline.by")

print(connect.message_multi_channel("VIBER_SMS", {
    "msisdn": "375XXXXXXXXX",
    "text": "Default text",
    "channels": [{
        "target": "ViberTarget",
        "options": {
            "ttl": "120",
            "img": "https://example.com/image.png",
            "caption": "Text on button",
            "action": "https://example.com"
        }
    }, {
        "target": "SmsTarget"
    }]
}))
```

### messages_single_channel
Для отправки нескольких сообщений через один канал нужно воспользоваться методом messages_single_channel (сообщение будет отправлено только через канал sms или viber). Который принимает два параметра:
  - channel, данный параметр может принимать значения "viber" или "sms"
  - body, принимает параметры, которые будут использоваться для отправки сообщений

Пример для отправки SMS:
```python
import smsline


connect = smsline.Smsline("username", "password", "https://api.smsline.by")

print(connect.messages_single_channel("sms", {
    "target": "target",
    "text": "Default text",
    "messages": [{
        "msisdn": "375XXXXXXXX1"
    }, {
        "msisdn": "375XXXXXXXX2",
        "text": "Custom text 1"
    }, {
        "msisdn": "375XXXXXXXX3",
        "text": "Custom text 2"
    }]
}))
```

Отправка сообщений через "viber" канал происходит аналогичным образом.

### messages_multi_channel
Для отправки нескольких сообщений через мультиканал канал, необходимо воспользоваться методом messages_multi_channel. Который принимает два параметра:
  - channel, данный параметр может принимать значения "viber" или "sms"
  - body, принимает параметры, которые будут использоваться для отправки сообщений

Пример для отправки сообщений через multi канал:
```python
import smsline


connect = smsline.Smsline("username", "password", "https://api.smsline.by")

print(connect.messages_multi_channel("VIBER_SMS", {
    "messages": [{
        "msisdn": "375XXXXXXXX1"
    }, {
        "msisdn": "375XXXXXXXX2"
    }, {
        "msisdn": "375XXXXXXXX3"
    }],
    "channels": [{
        "target": "ViberTarget",
        "text": "viber text",
        "options": {
            "ttl": "120",
            "img": "https://example.com/image.png",
            "caption": "Text on button",
            "action": "https://example.com"
        }
    }, {
        "target": "SmsTarget",
        "text": "sms text"
    }]
}))
```

### message_status
Возвращает статус отправленного сообщения. Принимает значение id_message.
```python
import smsline


connect = smsline.Smsline("username", "password", "https://api.smsline.by")

print(connect.message_status("00000000-0000-0000-0000-000000000000"))
```

### messages_status
Возвращает статусы всех сообщений попавших под выбранный фильтр. Принимает один параметр, который описывает какие сообщения необходимо вернуть.
```python
import smsline


connect = smsline.Smsline("username", "password", "https://api.smsline.by")

print(connect.messages_status({
    "target": "Target",
    "msisdn": "375XXXXXXXXX",
    "message_state": "2",
    "date_start": "YYYY-MM-DD HH:mm:ss",
    "date_end": "YYYY-MM-DD HH:mm:ss" #разница не более одних суток
}))
```