import requests
import hashlib
import hmac
import json
from urllib import parse


class Smsline:
    def __init__(self, login, password, url):
        self.__login = login
        self.__password = password
        self.__url = url

    def __send_get(self, url, signature):
        headers = {
            "User-Agent": "SMSLine-Python User: {0}".format(self.__login),
            "Authorization-User": self.__login,
            "Authorization": "Bearer {0}".format(signature)
        }

        res = requests.get(
            url,
            headers=headers
        )

        return {"Status": res.status_code, "Text": res.text}

    def __send_post(self, url, body, signature):
        headers = {
            "User-Agent": "SMSLine-Python User: {0}".format(self.__login),
            "Authorization-User": self.__login,
            "Authorization": "Bearer {0}".format(signature)
        }

        res = requests.post(
            url,
            headers=headers,
            json=body
        )

        return {"Status": res.status_code, "Text": res.text}

    def message_single_channel(self, channel, body):
        url = "{0}/v3/messages/single/{1}".format(self.__url, channel)
        data = json.dumps(body)

        signature = hmac.new(
            bytes(self.__password, "utf8"),
            "messagessingle{0}{1}".format(channel, data).encode(),
            hashlib.sha256
        ).hexdigest()

        return self.__send_post(url, body, signature)

    def message_multi_channel(self, channel, body):
        url = "{0}/v3/messages/multi/single/{1}".format(self.__url, channel)
        data = json.dumps(body)

        signature = hmac.new(
            bytes(self.__password, "utf8"),
            "messagesmultisingle{0}{1}".format(channel, data).encode(),
            hashlib.sha256
        ).hexdigest()

        return self.__send_post(url, body, signature)

    def messages_single_channel(self, channel, body):
        url = "{0}/v3/messages/bulk/{1}".format(self.__url, channel)
        data = json.dumps(body)

        signature = hmac.new(
            bytes(self.__password, "utf8"),
            "messagesbulk{0}{1}".format(channel, data).encode(),
            hashlib.sha256
        ).hexdigest()

        return self.__send_post(url, body, signature)

    def messages_multi_channel(self, channel, body):
        url = "{0}/v3/messages/multi/bulk/{1}".format(self.__url, channel)
        data = json.dumps(body)

        signature = hmac.new(
            bytes(self.__password, "utf8"),
            "messagesmultibulk{0}{1}".format(channel, data).encode(),
            hashlib.sha256
        ).hexdigest()

        return self.__send_post(url, body, signature)

    def message_status(self, message_id):
        url = "{0}/v3/messages/{1}".format(self.__url, message_id)

        signature = hmac.new(
            bytes(self.__password, "utf8"),
            "messages{0}".format(message_id).encode(),
            hashlib.sha256
        ).hexdigest()

        return self.__send_get(url, signature)

    def messages_status(self, params):
        url = "{0}/v3/messages?{1}".format(self.__url, parse.urlencode(params))
        for_signature = "messages"
        for k, v in sorted(params.items()):
            for_signature += "{0}{1}".format(k, v)

        signature = hmac.new(
            bytes(self.__password, "utf8"),
            for_signature.encode(),
            hashlib.sha256
        ).hexdigest()

        return self.__send_get(url, signature)
