# SMS-line HTTP API 3.0.0

Для работы модуля необходимо установить дополнительную библиотеку:
```sh
$ composer require rmccue/requests
```

### MessageSingleChannel
Для отправки одного сообщения через один канал нужно воспользоваться методом MessageSingleChannel (сообщение будет отправлено только через канал sms или viber). Который принимает два параметра:
  - channel, данный параметр может принимать значения "viber" или "sms"
  - body, принимает параметры которые будут использоваться для отправки сообщения

Пример для отправки SMS:
```php
<?php
include 'smsline.php';

$smsline = new Smsline("username", "password", "https://api.smsline.by");

echo $smsline -> MessageSingleChannel(
    "sms", 
    array(
        "target" => "Target",
        "msisdn" => "375XXXXXXXXX",
        "text" => "some text",
        "external_id" => "000000000",
        "callback_url" => "https://example.com/callback"
    )
);
?>
```

Пример для отправки Viber сообщения:
```php
<?php
include 'smsline.php';

$smsline = new Smsline("username", "password", "https://api.smsline.by");

echo $smsline -> MessageSingleChannel(
    "viber", 
    array(
        "target" => "Target",
        "msisdn" => "375XXXXXXXXX",
        "text" => "viber message",
        "options" => array(
            "ttl" => 120,
            "img" => "https://example.com/image.png",
            "caption" => "Text on button",
            "action" => "https://example.com"
        )
    )
);
?>
```

### MessageMultiChannel
Для отправки одного сообщения через мультиканал нужно воспользоваться методом MessageMultiChannel (сообщение будет отправлено через первый указанный канал, в случае неудачи повторная отправка произойдет через второй указанный канал). Который принимает два параметра:
  - channel, данный параметр может принимать единственное значение "VIBER_SMS"
  - body, принимает параметры которые будут использоваться для отправки сообщения

Пример для отправки сообщения через multi канал:
```php
<?php
include 'smsline.php';

$smsline = new Smsline("username", "password", "https://api.smsline.by");

echo $smsline -> MessageMultiChannel(
    "VIBER_SMS", 
    array(
        "msisdn" => "375XXXXXXXXX",
        "text" => "Default text",
        "channels" => array(
            array(
                "target" => "Viber target",
                "options" => array(
                    "ttl" => 120,
                    "img" => "https://example.com/image.png",
                    "caption" => "Text on button",
                    "action" => "https://example.com"
                )
            ),
            array(
                "target" => "SmsTarget"
            )
        )
    )
);
?>
```

### MessagesSingleChannel
Для отправки нескольких сообщений через один канал нужно воспользоваться методом MessagesSingleChannel (сообщения будут отправлены только через канал sms или viber). Который принимает два параметра:
  - channel, данный параметр может принимать значения "viber" или "sms"
  - body, принимает параметры которые будут использоваться для отправки сообщений

Пример для отправки SMS:
```php
<?php
include 'smsline.php';

$smsline = new Smsline("username", "password", "https://api.smsline.by");

echo $smsline->MessagesSingleChannel(
    "sms",
    array(
        "target" => "target",
        "text" => "Default text",
        "messages" => array(
            array(
                "msisdn" => "375XXXXXXXX1"
            ),
            array(
                "msisdn" => "375XXXXXXXX2",
                "text" => "Custom text 1"
            ),
            array(
                "msisdn" => "375XXXXXXXX3",
                "text" => "Custom text 2"
            )
        )
    )
);
?>
```

Отправка сообщений через "viber" канал произходит аналогичным образом.

### MessagesMultiChannel
Для отправки нескольких сообщений через мультиканал канал, необходимо воспользоваться методом MessagesMultiChannel. Который принимает два параметра:
  - channel, данный параметр может принимать значения "viber" или "sms"
  - body, принимает параметры которые будут использоваться для отправки сообщений

Пример для отправки сообщений через multi канал:
```php
<?php
include 'smsline.php';

$smsline = new Smsline("username", "password", "https://api.smsline.by");

echo $smsline->MessagesMultiChannel(
    "VIBER_SMS",
    array(
        "messages" => array(
            array(
                "msisdn" => "375XXXXXXXX1"
            ),
            array(
                "msisdn" => "375XXXXXXXX2"
            ),
            array(
                "msisdn" => "375XXXXXXXX3"
            )
        ),
        "channels" => array(
            array(
                "target" => "Viber target",
                "text" => "viber text",
                "options" => array(
                    "ttl" => 120,
                    "img" => "https://example.com/image.png",
                    "caption" => "Text on button",
                    "action" => "https://example.com"
                )
            ),
            array(
                "target" => "SmsTarget",
                "text" => "sms text",
            )
        )
    )
);
?>
```

### MessageStatus
Возвращает статус отправленного сообщения. Принимает значение id_message.
```php
<?php
include 'smsline.php';

$smsline = new Smsline("username", "password", "https://api.smsline.by");

echo $smsline->MessageStatus("00000000-0000-0000-0000-000000000000");
?>
```

### MessagesStatuses
Возвращает статусы всех сообщений попавших под выбранный фильтр. Принимает один параметр, который описывает какие сообщения необходимо вернуть.

```php
<?php
include 'smsline.php';

$smsline = new Smsline("username", "password", "https://api.smsline.by");

echo $smsline->MessagesStatuses(
    array(
        "date_start" => "YYYY-MM-DD HH:mm:ss",
        "date_end" => "YYYY-MM-DD HH:mm:ss", //разница не более одних суток
        "target" => "BambooGroup",
        "msisdn" => "375299862250",
        "message_state" => "2"
    )
);
?>
```

### Возможные ошибки
При возникновении ошибки "curl error 23", небходимо в файле 
```bash
vendor/rmccue/requests/library/Requests/Transport/cURL.phpvendor/rmccue/requests/library/Requests/Transport/cURL.php
```
исправить строку:
```php
if (!function_exists(‘curl_init’) || !function_exists(‘curl_exec’)) {
```
на
```php
if (true || !function_exists('curl_init') || !function_exists('curl_exec')) {
```
